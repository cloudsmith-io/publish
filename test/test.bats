#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/publish"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Test Python publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="python" \
        -e PACKAGE_PATH="test/resources/python-test.tar.gz" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test NPM publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="npm" \
        -e PACKAGE_PATH="test/resources/npm-test.tgz" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test Debian publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="deb" \
        -e PACKAGE_PATH="test/resources/deb-test.deb" \
        -e PACKAGE_DISTRIBUTION="ubuntu/xenial" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test Debian publish fails without distribution" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="deb" \
        -e PACKAGE_PATH="test/resources/deb-test.deb" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "Test Alpine publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="alpine" \
        -e PACKAGE_PATH="test/resources/alpine-test.apk" \
        -e PACKAGE_DISTRIBUTION="alpine/v3.9" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test Alpine publish fails without distribution" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="alpine" \
        -e PACKAGE_PATH="test/resources/alpine-test.apk" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "Test Maven publish w/ POM" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="maven" \
        -e PACKAGE_PATH="test/resources/maven-test.jar" \
        -e PACKAGE_MAVEN_POM_FILE="test/resources/maven-test.pom" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test Maven publish w/ GAV" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="maven" \
        -e PACKAGE_PATH="test/resources/maven-test.jar" \
        -e PACKAGE_MAVEN_GROUP_ID="io.cloudsmith" \
        -e PACKAGE_MAVEN_ARTIFACT_ID="maven-test" \
        -e PACKAGE_MAVEN_VERSION="1.0.0-SNAPSHOT" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test Cargo publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="cargo" \
        -e PACKAGE_PATH="test/resources/cargo-test.crate" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test Composer publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="cargo" \
        -e PACKAGE_PATH="test/resources/composer-test.phar" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test Helm publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="helm" \
        -e PACKAGE_PATH="test/resources/helm-test.tgz" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test Raw publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="raw" \
        -e PACKAGE_PATH="test/resources/raw-test.tgz" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test RPM publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="rpm" \
        -e PACKAGE_PATH="test/resources/rpm-test.rpm" \
        -e PACKAGE_DISTRIBUTION="fedora/28" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test RPM publish fails without distribution" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="rpm" \
        -e PACKAGE_PATH="test/resources/rpm-test.rpm" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "Test Ruby publish" {
    run docker run \
        -e CLOUDSMITH_REPOSITORY="cloudsmith/examples" \
        -e CLOUDSMITH_API_KEY="$CLOUDSMITH_API_KEY" \
        -e PACKAGE_FORMAT="ruby" \
        -e PACKAGE_PATH="test/resources/ruby-test.gem" \
        -e DRY_RUN="true" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}
