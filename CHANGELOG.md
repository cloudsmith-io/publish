# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.2

- patch: Allow version to apply to all package types.

## 0.3.1

- patch: Update pipe's metadata according to new Bitbucket Pipes structure.

## 0.3.0

- minor: Added Maven GAV and POM-less uploads support.

## 0.2.0

- minor: Added support for npm dist-tag

## 0.1.1

- patch: Fix docker image name and version

## 0.1.0

- minor: Initial release
