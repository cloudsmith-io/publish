FROM alpine:3.8

RUN apk update && apk add bash python3
RUN pip3 install cloudsmith-cli

COPY pipe /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
