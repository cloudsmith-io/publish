#!/usr/bin/env bash
#
# Publish packages to your Cloudsmith repositories
#

source "$(dirname "$0")/common.sh"

# Required parameters
CLOUDSMITH_REPOSITORY=${CLOUDSMITH_REPOSITORY:?'CLOUDSMITH_REPOSITORY variable missing.'}
CLOUDSMITH_API_KEY=${CLOUDSMITH_API_KEY:?'CLOUDSMITH_API_KEY variable missing.'}
PACKAGE_FORMAT=${PACKAGE_FORMAT:?'PACKAGE_FORMAT variable missing.'}
PACKAGE_PATH=${PACKAGE_PATH:?'PACKAGE_PATH variable missing.'}

# use `ls` to resolve any shell globs in PACKAGE_PATH
PACKAGE_PATH="$(ls $PACKAGE_PATH)"


# Handle optional arguments
ADDITIONAL_ARGS=

if [[ "${REPUBLISH}" == "true" ]]; then
  ADDITIONAL_ARGS+="--republish"
fi

if [[ "${DRY_RUN}" == "true" ]]; then
  ADDITIONAL_ARGS+=" --dry-run"
fi

if [[ "${DEBUG}" == "true" ]]; then
  ADDITIONAL_ARGS+=" --verbose"
fi

if [[ ! -z "${VERSION}" ]]; then
  ADDITIONAL_ARGS+=" --version ${VERSION}"
fi

# Handle per-format required arguments
if [[ "${PACKAGE_FORMAT}" == "alpine" || "${PACKAGE_FORMAT}" == "deb" || "${PACKAGE_FORMAT}" == "rpm" ]]; then
  CLOUDSMITH_REPOSITORY+="/${PACKAGE_DISTRIBUTION:?'PACKAGE_DISTRIBUTION variable missing.'}"
fi

if [[ "${PACKAGE_FORMAT}" == "maven" ]]; then
  if [[ ! -z "${PACKAGE_MAVEN_POM_FILE}" || ! -z "${PACKAGE_POM_FILE}" ]]; then
    ADDITIONAL_ARGS+=" --pom-file ${PACKAGE_MAVEN_POM_FILE:-$PACKAGE_POM_FILE}"
  fi
  if [[ ! -z "${PACKAGE_MAVEN_GROUP_ID}" ]]; then
    ADDITIONAL_ARGS+=" --group-id ${PACKAGE_MAVEN_GROUP_ID}"
  fi
  if [[ ! -z "${PACKAGE_MAVEN_ARTIFACT_ID}" ]]; then
    ADDITIONAL_ARGS+=" --artifact-id ${PACKAGE_MAVEN_ARTIFACT_ID}"
  fi
fi

if [[ "${PACKAGE_FORMAT}" == "npm" ]]; then
  if [[ ! -z "${PACKAGE_NPM_DIST_TAG}" ]]; then
    ADDITIONAL_ARGS+=" --npm-dist-tag ${PACKAGE_NPM_DIST_TAG}"
  fi
fi


# Upload package using the Cloudsmith CLI
run cloudsmith push "$PACKAGE_FORMAT" $ADDITIONAL_ARGS "$CLOUDSMITH_REPOSITORY" "$PACKAGE_PATH"

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
