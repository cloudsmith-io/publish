# Bitbucket Pipelines Pipe: Cloudsmith Publish

Publish packages to your Cloudsmith repositories. This pipe uses the [Cloudsmith CLI](https://github.com/cloudsmith-io/cloudsmith-cli) to perform all uploads.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: cloudsmith-io/publish:0.3.2
    variables:
      CLOUDSMITH_REPOSITORY: "<string>"
      CLOUDSMITH_API_KEY: "<string>"
      PACKAGE_FORMAT: "<string>"
      PACKAGE_PATH: "<string>"
      # DEBUG: "<string>" # Optional
      # REPUBLISH: "<boolean>" # Optional
      # PACKAGE_DISTRIBUTION: "<string>" # Optional (alpine/debian/redhat)
      # PACKAGE_MAVEN_POM_FILE: "<string>" # Optional (maven)
      # PACKAGE_MAVEN_GROUP_ID: "<string>" # Optional (maven)
      # PACKAGE_MAVEN_ARTIFACT_ID: "<string>" # Optional (maven)
      # PACKAGE_MAVEN_VERSION: "<string>" # Optional (maven)
      # PACKAGE_NPM_DIST_TAG: "<string>" # Optional (npm)
```
## Variables

| Variable              | Usage                                                       | Example |
| --------------------- | ----------------------------------------------------------- | ------- |
| CLOUDSMITH_REPOSITORY (*) | The Cloudsmith repository to which artifacts will be uploaded. | `"cloudsmith/examples"` |
| CLOUDSMITH_API_KEY (*) | The API Key used to authenticate with Cloudsmith. | `"aaabbbcccdddeee"` |
| PACKAGE_FORMAT (*) | Format of the artifact being uploaded. | `"python"` |
| PACKAGE_PATH (*) | Path to the file to be uploaded (accepts shell globs). | `"dist/*.whl"` |
| DEBUG | Turn on extra debug information. Default: `"false"`. | `"true"` |
| REPUBLISH | Allow publishing a package that overwrites an existing version on Cloudsmith. Default: `"false"`. | `"true"` |
| PACKAGE_DISTRIBUTION | `distro/release` for which the package is built (valid/required only for `deb`, `rpm` and `alpine`). | `"fedora/28"` |
| PACKAGE_MAVEN_POM_FILE | Path to the POM file associated with a maven release (valid only for `maven`). | `"build/pom.xml"` |
| PACKAGE_MAVEN_GROUP_ID | The group ID of the package (valid only for `maven`, overides or replaces POM file). | `"io.cloudsmith"` |
| PACKAGE_MAVEN_ARTIFACT_ID | The artifact ID of the package (valid only for `maven`, overrides or replaces POM file). | `"csm-libs"` |
| PACKAGE_MAVEN_VERSION | The version of the package (valid only for `maven`, overrides or replaces POM file). | `"1.0.0-SNAPSHOT"` |
| PACKAGE_NPM_DIST_TAG | The dist-tag to use for npm packages (valid for `npm`; if not specified, the default is "latest") | `"stable"` |

_(*) = required variable._

## Prerequisites

An active Cloudsmith account and API key are required.

## Examples

Basic example:

```yaml
script:
- pipe: cloudsmith-io/publish:0.3.2
  variables:
    CLOUDSMITH_REPOSITORY: 'cloudsmith/examples'
    CLOUDSMITH_API_KEY: $CLOUDSMITH_API_KEY
    PACKAGE_FORMAT: 'python'
    PACKAGE_PATH: 'dist/*.whl'
```

Advanced example #1 (npm w/ tags):

```yaml
script:
- pipe: cloudsmith-io/publish:0.3.2
  variables:
    CLOUDSMITH_REPOSITORY: 'cloudsmith/examples'
    CLOUDSMITH_API_KEY: $CLOUDSMITH_API_KEY
    PACKAGE_FORMAT: 'npm'
    PACKAGE_NPM_DIST_TAG: 'stable'
    PACKAGE_PATH: '*.tgz'
    REPUBLISH: 'true'
    DEBUG: 'true'
```

Advanced example #2 (maven w/ pom):

```yaml
script:
- pipe: cloudsmith-io/publish:0.3.2
  variables:
    CLOUDSMITH_REPOSITORY: 'cloudsmith/examples'
    CLOUDSMITH_API_KEY: $CLOUDSMITH_API_KEY
    PACKAGE_FORMAT: 'maven'
    PACKAGE_PATH: 'target/csm-libs-1.0.0.jar'
    PACKAGE_MAVEN_POM_FILE: 'target/csm-libs-1.0.0.pom'
    REPUBLISH: 'true'
    DEBUG: 'true'
```

Advanced example #3 (maven w/ GAV):

```yaml
script:
- pipe: cloudsmith-io/publish:0.3.2
  variables:
    CLOUDSMITH_REPOSITORY: 'cloudsmith/examples'
    CLOUDSMITH_API_KEY: $CLOUDSMITH_API_KEY
    PACKAGE_FORMAT: 'maven'
    PACKAGE_PATH: 'target/csm-libs-1.0.0-SNAPSHOT.jar'
    PACKAGE_MAVEN_GROUP_ID: 'io.cloudsmith'
    PACKAGE_MAVEN_ARTIFACT_ID: 'csm-libs'
    PACKAGE_MAVEN_VERSION: '1.0.0-SNAPSHOT'
    REPUBLISH: 'true'
    DEBUG: 'true'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request contact [Cloudsmith support](https://cloudsmith.io/contact/).

If you’re reporting an issue, please include:

- The version of the pipe.
- Relevant logs and error messages.
- Steps to reproduce.
